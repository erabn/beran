##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität Münche)           #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017

import numpy as np

class Network(object):
    """
    Network in a nutshell contains nodes. It also knows which nodes are roots
    and which are leaves
    """
    def __init__(self, nodes: list=[], roots: list=[], leaves: list=[], dictionary: dict = {}):
        self.nodes = nodes
        self.roots = roots
        self.leaves = leaves
        self.dictionary = {}
        
    def filldictionary(self):
        for i in range(0,len(self.nodes)):
            self.dictionary.update({self.nodes[i].name:i})


    def findchildren(self):
        """ 
        Assigns a list of children names to each node
        """
        for inode in self.nodes:
            self.nodes[self.dictionary[inode.name]].children = []
            for jnode in self.nodes:
                if not any(jnode.name == members for members in self.nodes[self.dictionary[inode.name]].children):
                    for parent in jnode.parents:
                        if type(parent) == dict :
                            parentname = list(parent.keys())[0]
                        else:
                            parentname = parent
                        if inode.name == parentname:
                            self.nodes[self.dictionary[inode.name]].children.append(jnode.name)
                            break

    def setpredecessorsunbarren(self,inode_name):
        indx = self.dictionary[inode_name];
        
        for iparent in self.nodes[indx].parents:
            if type(iparent) == dict:
                iparent_name = list(iparent.keys())[0]
            else: 
                iparent_name = iparent

            indx2 = self.dictionary[iparent_name]
            if self.nodes[indx2].barren == 'barren':
                self.nodes[indx2].barren = 'unbarren'
                self.setpredecessorsunbarren(iparent_name)


    def isbarren(self):
        """
        Assigns a field barren to each node
        A node is barren if neither itself nor any of its descendants have recived any evidence
        """
        for inode in self.nodes: #Set all nodes barren
            inode.barren = 'barren'
        for inode in self.nodes:
            if inode.evidence: #Set all nodes that have received evidence and their predecessors unbarren
                inode.barren = 'unbarren'
                self.setpredecessorsunbarren(inode.name)

    def exploredescendants(self,inode_name,randchild,interfcn,constrfcn) -> tuple:
        """
        Goes through the descendants of a node inode_name until all the next ranodm cilds are found 
        In this course records all functions between the node and its next random child (in interfcn)
        and additionally also those functions that have received inequality evidence (constrfcn)
        """
        indx = self.dictionary[inode_name]
        for ichild in self.nodes[indx].children:
            indx2 = self.dictionary[ichild]
            if self.nodes[indx2].barren == 'unbarren':
                if self.nodes[indx2].__class__.__name__ in ('DistributionNode','DiscreteCPTNode'): # non function nodes are directly added to the random ancestors
                    randchild.append(self.nodes[indx2])
                else:
                    interfcn.append(self.nodes[indx2])
                    if self.nodes[indx2].evidence:
                        if self.nodes[indx2].evidence.type == '=':
                            print('ERROR: Equality evidence for functions is not supported')
                            import sys
                            sys.exit()
                        else:
                            constrfcn.append(self.nodes[indx2])

                    self.exploredescendants(ichild,randchild,interfcn,constrfcn)
        return (randchild,interfcn,constrfcn)

    def makecondpdfprop(self,inode_name):
        """
        Returns a function of the node self that is proportional toits pdf conditional on its Markov blanket
        """
        idx = self.dictionary[inode_name]
        node_self = self.nodes[idx]
        randchild = []
        interfcn  = []
        constrfcn = []
        (randchild,interfcn,constrfcn) = self.exploredescendants(inode_name,randchild,interfcn,constrfcn)
        def condpdfprop(sample):
            logf = 0
            if node_self.evidence:                
                logf += np.log(node_self.evidence.evidencefulfilled(sample,node_self))
            if logf>float('-inf'):
                x = sample[inode_name]
                logf += np.log(node_self.distribution.pdf(x,sample))
                for i_fcn in interfcn:
                    sample[i_fcn.name] = i_fcn.func(sample)
                for i_constr in constrfcn:
                    logf += np.log(i_constr.evidence.evidencefulfilled(sample,i_constr))
                    if logf == float('-inf'):
                        break
                if logf>float('-inf'):            
                    for i_randchild in randchild:    
                        x_ch = sample[i_randchild.name]                           
                        logf += np.log(i_randchild.distribution.pdf(x_ch,sample))
            return logf
        return condpdfprop 


    def makecondpmf(self,inode_name):
        """ 
        Returns a function of the node self that is proportional to its pdf conditional on its Markov blanket
        """ 
        idx = self.dictionary[inode_name]
        node_self = self.nodes[idx]
        randchild = []
        interfcn  = []
        constrfcn = []
        (randchild,interfcn,constrfcn) = self.exploredescendants(inode_name,randchild,interfcn,constrfcn)
        def condpmf(sample):
            x = sample[inode_name]   
            logf = np.log(node_self.distribution.pdf(x,sample))
            lognorm_const = 0              
            if node_self.evidence:           
                logf += np.log(node_self.evidencefulfilled(sample,node_self))
            for i_fcn in interfcn:
                sample[i_fcn.name] = i_fcn.func(sample)
            for i_constr in constrfcn:
                logf += np.log(i_constr.evidence.evidencefulfilled(sample,i_constr))
                if logf == float('-inf'):
                    break   
            if logf > float('-inf'):                
                if randchild:                         
                    for i_randchild in randchild:
                        x_ch = sample[i_randchild.name]                           
                        logf += np.log(i_randchild.distribution.pdf(x_ch,sample))
                    lognorm_const_i = []
                    samplecopy = sample.copy()
                    for istates in self.nodes[idx].distribution.params['states']:
                        samplecopy[inode_name] = istates                                             
                        lognorm_const_i_temp = np.log(self.nodes[idx].distribution.pdf(istates,samplecopy))
                        for i_fcn in interfcn:
                            samplecopy[i_fcn.name] = i_fcn.func(samplecopy)
                        for i_constr in constrfcn:                          
                            lognorm_const_i_temp += np.log(i_constr.evidence.evidencefulfilled(samplecopy,i_constr))
                            if lognorm_const_i_temp == float('-inf'):
                                break    
                        count = 0 
                        if lognorm_const_i_temp > float('-inf'):
                            for i_randchild in randchild:
                                count += 1
                                x_ch = samplecopy[i_randchild.name]                            
                                lognorm_const_i_temp += np.log(i_randchild.distribution.pdf(x_ch,samplecopy))
                        lognorm_const_i.append(lognorm_const_i_temp)                
                    lognorm_const_i = np.sort(lognorm_const_i)
                    lognorm_const = lognorm_const_i[-1]
                    for i_lognorm_const_i in lognorm_const_i[:-1]:
                        lognorm_const += np.log(1+np.exp(i_lognorm_const_i-lognorm_const))
                f_normalized = np.exp(logf-lognorm_const)
            else:
                f_normalized = 0
            return f_normalized
        return condpmf 

    def setGibbsSampler(self,inode):
        idx = self.dictionary[inode.name]
        if inode.__class__.__name__ ==  'DistributionNode':
            if not hasattr(inode,'univariatesampler'):    
                self.nodes[idx].univariatesampler = 'slice'
                if not hasattr(inode,'typStepWidth'):
                    self.nodes[idx].typStepWidth = 1
            elif self.nodes[idx].univariatesampler == 'slice':
                 if not hasattr(inode,'typStepWidth'):
                    self.nodes[idx].typStepWidth = 1           
            elif self.nodes[idx].univariatesampler == 'MH':
                if not hasattr(inode,'StdPropPDF'):
                    self.nodes[idx].StdPropPDF = 1  
                self.nodes[idx].adaptive = True
                if not hasattr(inode,'AdaptionInitStdPropPDF'):
                    self.nodes[idx].AdaptionInitStdPropPDF = self.nodes[idx].StdPropPDF
                self.nodes[idx].AdaptionAfterChain = 10
                self.nodes[idx].AdaptionAccept1stStage = 0
                if not hasattr(self.nodes[idx],'AdaptionCount'):                
                    self.nodes[idx].AdaptionCount = 0               
                self.nodes[idx].AdaptionAcceptedSamples = 0
                self.nodes[idx].AdaptionTotalSamples = 0   
            else:
                print('ERROR: Univariate sampler (',self.nodes[idx].univariatesampler,') is not implemented')
                import sys
                sys.exit()
        if inode.__class__.__name__ ==  'DiscreteCPTNode':
            if not hasattr(inode,'univariatesampler'):    
                self.nodes[idx].univariatesampler = '-'
                self.nodes[idx].adaptive = False                
            elif self.nodes[idx].univariatesampler == 'MH':
                if not hasattr(inode,'transitionPropProb'):
                    self.nodes[idx].transitionPropProb = 0.5 # Transition propsal probability for discrete Nodes           
                if not hasattr(inode,'StdPropPDF'):
                    self.nodes[idx].StdPropPDF = 1  
                    self.nodes[idx].standardNormalSample = None
                self.nodes[idx].adaptive = True
                self.nodes[idx].AdaptionInitTransitionPropProb = self.nodes[idx].transitionPropProb
                if not hasattr(inode,'AdaptionInitStdPropPDF'):
                    self.nodes[idx].AdaptionInitStdPropPDF = self.nodes[idx].StdPropPDF
                self.nodes[idx].AdaptionAfterChain = 10
                self.nodes[idx].AdaptionAccept1stStage = 0                
                self.nodes[idx].AdaptionCount = 0               
                self.nodes[idx].AdaptionAcceptedSamples = 0
                self.nodes[idx].AdaptionTotalSamples = 0   
                self.nodes[idx].AcceptanceProbOnFailureSurface = None
            elif self.nodes[idx].univariatesampler == '-':
                pass
            else:
                print('ERROR: Univariate sampler (', self.nodes[idx].univariatesampler, ') is not implemented')
                import sys
                sys.exit()

    def findGibbsSamplingObj(self):
        """
        Returns a vector of names of objects that need to be iterated through to when Gibbs sampling
        i.e. distribution nodes (Single component), barren functions, Gibbs blocks
        """        
        self.ToBeSampled = [] # vector of objects that need to be sampled
        for inode in self.nodes:
            if hasattr(inode,'block'):
                if inode.block in self.ToBeSampled:
                    self.ToBeSampled[inode.block].append(inode) 
                else:
                    self.ToBeSampled.append(inode)   
            elif inode.__class__.__name__ ==  'DistributionNode':
                self.ToBeSampled.append(inode)
                self.setGibbsSampler(inode)
            elif inode.__class__.__name__ ==  'DiscreteCPTNode':
                self.ToBeSampled.append(inode)
                self.setGibbsSampler(inode)                                
            elif inode.__class__.__name__ == 'FunctionNode':
                self.ToBeSampled.append(inode)    
                #if inode.barren == 'barren':
        
    def adaptMHproposal(self,ichain):       
        if ichain > 0:           
            for iSampObj in self.ToBeSampled:
                if iSampObj.__class__.__name__ == 'DistributionNode':
                    if iSampObj.univariatesampler == 'MH':
                        if iSampObj.adaptive:
                            if (ichain+1)%iSampObj.AdaptionAfterChain == 0:
                                iSampObj.adaptUnivariateMHproposal()
                if iSampObj.__class__.__name__ == 'DiscreteCPTNode':
                    if iSampObj.univariatesampler == 'MH':
                        if iSampObj.adaptive:
                            if ichain%iSampObj.AdaptionAfterChain == 0:
                                iSampObj.adaptUnivariateMHproposal()