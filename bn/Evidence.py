##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität Münche)           #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017

import numpy as np

class Evidence(object):
    """
    Evidence of sample output for a node
    """
    def __init__(self, val: float, typ: str):
        """
        Constructor
        :param val: evidence value
        :param typ: type of evidence
        """
        self.value = val
        self.type = typ

    def evidencefulfilled(self,sample,node): 
        """
        checks for function, fcn if evidence is fulfilled by sample
        if yes returns fulfilled = 1 else fulfilled = 0
        """
        fulfilled = 0
        if node.__class__.__name__ == 'FunctionNode':
            curValue = node.func(sample)
        else:
            curValue = sample[node.name]

        if self.type == '=':
            if curValue == self.value:
                fulfilled = 1
                
        elif self.type == '<':
            if curValue < self.value:
                fulfilled = 1
        elif self.type == '>':
            if curValue > self.value:
                fulfilled = 1
        elif self.type == '<=':
            if curValue <= self.value:
                fulfilled = 1
        elif self.type == '>=':
            if curValue >= self.value:
                fulfilled = 1
        elif self.type == 'between':
            if curValue > self.value[0] and curValue < self.value[1]:
                fulfilled = 1
        
        return fulfilled


