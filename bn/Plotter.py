##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität München)          #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Hugo Rosero (hugo.rosero@tum.de)
# September 2017


from bn import Network
#from bn.Sampling.Sampling import Sampling
#import random
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

class Plotter(object):
    def __init__(self, network: Network, samples: dict={}):
        self.network = network
        self.samples=samples
        self.network.filldictionary()
        self.network.findchildren() 
        
 
    def histNodes(self,inode_name='all',bins=10,density=False,save=False):
        if inode_name=='all':
            #no arguments, plot all nodes
            for inode in self.network.nodes:
                self.histNodes(inode.name,bins,density,save)
                
        elif isinstance(inode_name,str):
            #user wants to plot only one node
            if self.isNodeCat(inode_name):
                self.plotHistCatNode(inode_name,bins,density,save)
            else:
                self.plotHistNumNode(inode_name,bins,density,save)
        else:
            #user inputs a list of node names
            n=len(inode_name)
            for i in range(0,n):
                self.histNodes(inode_name[i],bins,density,save)
            
    
    def scatterNodes(self,inode_list,save=False):
        if isinstance(inode_list[1],str) and len(inode_list)==2:
            #user wants to compare just 2 RVs
            inode_name1=inode_list[0]
            inode_name2=inode_list[1]
            if self.isNodeCat(inode_name1) and self.isNodeCat(inode_name2):
                print('WARNING: No scatter possible. Both nodes are categorical')
            elif self.isNodeCat(inode_name1):
                self.scatterCatNodes(inode_name1,inode_name2,save)
            elif self.isNodeCat(inode_name2):
                self.scatterCatNodes(inode_name2,inode_name1,save)
            else:
                self.scatterNumNodes(inode_name1,inode_name2,save)
        else:
            #user wants to compare multiple pairs of RVs
            n=len(inode_list)
            for i in range(0,n):
                self.scatterNodes(inode_list[i],save)
    
    def plotHistCatNode(self,inode_name,bins=10, density=False,save=False):
        sampx=self.samples[inode_name]
        myNodeID=self.network.dictionary[inode_name]
        myNodeStates=self.network.nodes[myNodeID].distribution.params['states']
        plt.figure()
        plt.clf()
        if density==True:
            nn=len(myNodeStates)
            n=len(sampx)
            x=range(1,len(myNodeStates)+1)
            theDict=dict(zip(x,[0]*nn))
            PrInc=1/n
            for i in range(0,n):
                ind=myNodeStates.index(sampx[i])+1
                theDict[ind]=theDict[ind]+PrInc
            ax = plt.subplot(1, 1, 1)
            plt.bar(list(theDict.keys()), theDict.values())
            ax.set_xticks([i for i in x])
            ax.set_xticklabels(myNodeStates)
            plt.ylabel('density')
        else:
            ax=sns.countplot(x=sampx,order=myNodeStates,facecolor=(65/256, 105/256, 225/256,1))
            plt.xlabel('state')

        plt.xlabel('state') 
        plt.setp(ax.get_xticklabels(), fontsize=14)
        plt.setp(ax.get_yticklabels(), fontsize=14)
        ax.xaxis.label.set_fontsize(20)
        ax.yaxis.label.set_fontsize(20)
        plt.title('Histogram samples node '+inode_name,fontsize=30)
        ax.set_facecolor((0.9, 0.9, 0.9))
        plt.title('Histogram samples node '+inode_name,fontsize=30)
        if save==True:
            filename='hist_node_'+inode_name+'.pdf'
            pp = PdfPages(filename)
            pp.savefig()
            pp.close()

                
    def plotHistNumNode(self,inode_name,bins=10, density=False,save=False):
        sampx=self.samples[inode_name]
        plt.figure()
        plt.clf()
        ax = plt.subplot(1, 1, 1)
        ax.set_facecolor((0.9, 0.9, 0.9))
        plt.hist(sampx,bins=bins,normed=density,facecolor=(21/256,47/256,239/256,1))
        plt.axvline(np.mean(sampx), color='r', linestyle='dashed', linewidth=1.5)
        #plt.axvline(np.mean(sampx)+np.std(sampx), color='g', linestyle='dashed', linewidth=1.5)
        #plt.axvline(np.mean(sampx)-np.std(sampx), color='g', linestyle='dashed', linewidth=1.5)
        strMean='%3.2lf' % (np.mean(sampx))
        strStd='%3.2lf' % (np.std(sampx))
        #plt.legend(label='mean = '+strMean+', std. dev. = '+strStd)
        #ax=plt.axes
        plt.setp(ax.get_xticklabels(), fontsize=14)
        plt.setp(ax.get_yticklabels(), fontsize=14)
        #plt.hist(sampx)
        plt.title('Histogram samples node '+inode_name+'\n mean = '+strMean+', std. dev. = '+strStd, fontsize=20)
        #plt.title(r'{\fontsize{30pt}{3em}\selectfont{}{Histogram samples node '+inode_name+'\r}{\fontsize{18pt}{3em}\selectfont{}mean = '+strMean+', std. dev. = '+strStd+'}')
        plt.xlabel('Values', fontsize=20)
        #plt.xticks(fontsize=14)
        if density==True:
            plt.ylabel('density', fontsize=20)
        else:
            plt.ylabel('count', fontsize=20)
        #plt.yticks(fontsize=14)
        if save==True:
            filename='hist_node_'+inode_name+'.pdf'
            pp = PdfPages(filename)
            pp.savefig()
            pp.close()    
    
    def scatterNumNodes(self,inode_name1,inode_name2,save=False):
        sampx=self.samples[inode_name1]
        sampy=self.samples[inode_name2]
        plt.figure()
        plt.clf()
        ax = plt.subplot(1, 1, 1)
        ax.set_facecolor((0.9, 0.9, 0.9))
        plt.scatter(sampx,sampy)
        plt.setp(ax.get_xticklabels(), fontsize=14)
        plt.setp(ax.get_yticklabels(), fontsize=14)
        plt.title('Scatter node '+inode_name1+' vs. node '+inode_name2, fontsize=30)
        plt.xlabel('Values for node '+inode_name1, fontsize=20)
        plt.ylabel('Values for node '+inode_name2, fontsize=20)
        if save==True:
            filename='scatter_'+inode_name1+'_vs_'+inode_name2+'.pdf'
            pp = PdfPages(filename)
            pp.savefig()
            pp.close()      

    def scatterCatNodes(self,inode_name1,inode_name2,save=False):
        sampx=self.samples[inode_name1]
        myNodeID=self.network.dictionary[inode_name1]
        myNodeStates=self.network.nodes[myNodeID].distribution.params['states']
        sampy=self.samples[inode_name2]
        plt.figure()
        plt.clf()
        ax=sns.swarmplot(x=sampx,y=sampy,order=myNodeStates,facecolor=(21/256,47/256,239/256,1))
        plt.setp(ax.get_xticklabels(), fontsize=14)
        plt.setp(ax.get_yticklabels(), fontsize=14)
        plt.title('Scatter node '+inode_name1+' vs. node '+inode_name2, fontsize=30)
        plt.xlabel('Values for node '+inode_name1, fontsize=20)
        plt.ylabel('Values for node '+inode_name2, fontsize=20)
        if save==True:
            filename='scatter_'+inode_name1+'_vs_'+inode_name2+'.pdf'
            pp = PdfPages(filename)
            pp.savefig()
            pp.close() 
    
    def isNodeCat(self,inode_name):
        #value=False
        myNodeID=self.network.dictionary[inode_name]
        inode=self.network.nodes[myNodeID]
        if inode.__class__.__name__ == 'DiscreteCPTNode':
            return True
        else:
            return False
    

        
        
        

