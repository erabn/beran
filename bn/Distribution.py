##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität Münche)           #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017

import numpy as np
from scipy import stats

class Distribution(object):
    """
    Distribution, which is used for generating node's sample
    """
    def __init__(self, icdf, pdf, cdf, params: dict = None):
        """
        Constructor
        :param icdf: inverse cumulative distribution function (icdf) function
        :param pdf: probability density function (pdf) function
        :param params: various parameters that might be used by icdf and pdf functions
        """
        if not callable(icdf):
            raise TypeError('icdf has to be a function')
        if not callable(pdf):
            raise TypeError('pdf has to be a function')
        if not callable(cdf):
            raise TypeError('cdf has to be a function')

        self.icdf = icdf
        self.pdf = pdf
        self.params = params


class CPTDistribution(Distribution):
    """
    Distribution based on conditional probability tables
    """
    def __init__(self, cpt: list, states: list, parents: list):
        """
        Constructor
        :param cpt: conditional probability table in a form of matrix
        :param states: possible sample value list
        """
        cpt_test = cpt[:]
        # CPT dimension check
        requiredSize = 1
        for iPar in parents:
            if type(iPar) == dict:
                NumStatesiPar = len(list(iPar.values())[0])
                requiredSize *= NumStatesiPar
                if len(cpt_test) != NumStatesiPar:
                    raise ValueError('The CPT does not correspond to the number of states in at least one dimension')
                cpt_test = cpt_test[0] 
        requiredSize *= len(states)
        CPTsize = self.recursive_num_CPTelements(cpt)
        if requiredSize != CPTsize:
            raise ValueError('The size of the CPT appears to be incorrect')

        params = {'cpt': cpt, 'states': states, 'parents': parents}

        # Little bit of mumbo-jumbo on referring class methods
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)
        
    def recursive_num_CPTelements(self,CPT):
        if type(CPT) == list:
            CPTsize = sum(self.recursive_num_CPTelements(subCPT) for subCPT in CPT)
            return CPTsize
        else:
            return 1

    def pdf(self,s,samples):
        # The pmf of the node self conditional on the current state of the parents 
        temp_pmf = self.params['cpt'][:]
        for ipar in self.params['parents']:
            if type(ipar) == dict:
                DiscrVarName = list(ipar.keys())[0]
                temp_pmf = temp_pmf[ipar[DiscrVarName].index(samples[DiscrVarName])]

        pmf_self = temp_pmf[self.params['states'].index(s)]

        if pmf_self.__class__.__name__ == 'function':
            pmf_self = pmf_self(samples)  

        return pmf_self


    def icdf(self,r,samples):
        # The pmf of the node self conditional on the current state of the parents
        temp_pmf = self.params['cpt'][:]
        for ipar in self.params['parents']:
            if ipar.__class__.__name__ == 'dict':
                DiscrVarName = list(ipar.keys())[0]
                temp_pmf = temp_pmf[ipar[DiscrVarName].index(samples[DiscrVarName])]

        count = 0

        while True:
            if temp_pmf[count].__class__.__name__ == 'function':
                temp_pmf[count] = temp_pmf[count](samples)
            if r <= sum(temp_pmf[:count+1]):
                x = self.params['states'][count]
                break
            count += 1                
        return x

    def cdf(self,s,samples):
        # The cdf of the node self conditional on the current state of the parents 
        # Assumes that the states are ordered 

        cdf_self = 0
        for iState in self.params['states']:
            temp_pmf = self.params['cpt'][:]
            for ipar in self.params['parents']:
                if type(ipar) == dict:
                    DiscrVarName = list(ipar.keys())[0]
                    temp_pmf = temp_pmf[ipar[DiscrVarName].index(samples[DiscrVarName])]

            pmf_self = temp_pmf[self.params['states'].index(s)]

            if pmf_self.__class__.__name__ == 'function':
                pmf_self = pmf_self(samples) 
            cdf_self += pmf_self

            if iState == s:
                break

        return cdf_self


class NormalDistribution(Distribution):
    """
    Normal based distribution
    """
    def __init__(self, params: dict):
        # TODO: set a formal list of parameter resolving
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)

    def resolve_parameters(self, samples: list) -> tuple:
        """
        Distribution based function for parameter resolving
        :param samples: list of parent nodes' samples
        :return: tuple: location and scale
        """
        location = self.params['loc']
        if callable(self.params['loc']):
            location = self.params['loc'](samples)

        scale = self.params['scale']
        if callable(self.params['scale']):
            scale = self.params['scale'](samples)

        return location, scale

    def icdf(self, r, samples: list) -> float:
        """
        Wrapper for SciPy's norm icdf function
        :param r: uniformly random sample
        :param samples: list of parent nodes' samples
        :return: sample value
        """
        location, scale = self.resolve_parameters(samples)
        return stats.norm.ppf(r, loc=location, scale=scale)

    def pdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's norm pdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: pdf value
        """
        location, scale = self.resolve_parameters(samples)
        return stats.norm.pdf(s, loc=location, scale=scale)

    def cdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's norm cdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: cdf value
        """
        location, scale = self.resolve_parameters(samples)
        return stats.norm.cdf(s, loc=location, scale=scale)

class TruncatedNormalDistribution(Distribution):
    """
    Normal based distribution
    """
    def __init__(self, params: dict):
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)

    def resolve_parameters(self, samples: list) -> tuple:
        """
        Distribution based function for parameter resolving
        :param samples: list of parent nodes' samples
        :return: tuple: lower bound a, upper bound b, location and scale
        """
        a = self.params['a']
        if callable(self.params['a']):
            a = self.params['a'](samples)

        b = self.params['b']
        if callable(self.params['b']):
            b = self.params['b'](samples)

        location = self.params['loc']
        if callable(self.params['loc']):
            location = self.params['loc'](samples)

        scale = self.params['scale']
        if callable(self.params['scale']):
            scale = self.params['scale'](samples)

        return a, b, location, scale

    def icdf(self, r, samples: list) -> float:
        """
        Wrapper for SciPy's truncnorm icdf function
        :param r: uniformly random sample
        :param samples: list of parent nodes' samples
        :return: sample value
        """
        a, b, location, scale = self.resolve_parameters(samples)                          
        return stats.truncnorm.ppf(r, a=a, b=b, loc=location, scale=scale)

    def pdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's truncnorm pdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: pdf value
        """
        a, b, location, scale = self.resolve_parameters(samples)
        return stats.truncnorm.pdf(s, a=a, b=b, loc=location, scale=scale)

    def cdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's truncnorm cdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: cdf value
        """
        a, b, location, scale = self.resolve_parameters(samples)
        return stats.truncnorm.cdf(s, a=a, b=b, loc=location, scale=scale)

class BetaDistribution(Distribution):
    """
    Beta based distribution
    """
    def __init__(self, params: dict):
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)

    def resolve_parameters(self, samples: list) -> tuple:
        """
        Distribution based function for parameter resolving
        :param samples: list of parent nodes' samples
        :return: tuple: location and scale
        """
        a = self.params['a']
        if callable(self.params['a']):
            a = self.params['a'](samples)

        b = self.params['b']
        if callable(self.params['b']):
            b = self.params['b'](samples)

        return a, b

    def icdf(self, r, samples: list) -> list:
        """
        Wrapper for SciPy's beta icdf function
        :param r: uniformly random sample
        :param samples: list of parent nodes' samples
        :return: sample value
        """
        a, b = self.resolve_parameters(samples)
        return stats.beta.ppf(r, a=a, b=b)

    def pdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's beta pdf function
        :param s: sample value
        :param samples: list of parents nodes' samples
        :return: pdf value
        """
        a, b = self.resolve_parameters(samples)

        return stats.beta.pdf(s, a=a, b=b)

    def cdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's beta cdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: cdf value
        """
        a, b = self.resolve_parameters(samples)
        return stats.norm.cdf(s, a=a, b=b)

class UniformDistribution(Distribution):
    """
    Beta based distribution
    """
    def __init__(self, params: dict):
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)

    def resolve_parameters(self, samples: list) -> tuple:
        """
        Distribution based function for parameter resolving
        :param samples: list of parent nodes' samples
        :return: tuple: location and scale
        """
        location = self.params['loc']
        if callable(self.params['loc']):
            location = self.params['loc'](samples)

        scale = self.params['scale']
        if callable(self.params['scale']):
            scale = self.params['scale'](samples)

        return location, scale

    def icdf(self, r, samples: list) -> list:
        """
        Wrapper for SciPy's uniform icdf function
        :param r: uniformly random sample
        :param samples: list of parent nodes' samples
        :return: sample value
        """
        location, scale = self.resolve_parameters(samples)
        return stats.uniform.ppf(r, loc=location, scale=scale)

    def pdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's uniform pdf function
        :param s: sample value
        :param samples: list of parents nodes' samples
        :return: pdf value
        """
        location, scale = self.resolve_parameters(samples)

        return stats.uniform.pdf(s, loc=location, scale=scale)

    def cdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's uniform cdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: cdf value
        """
        location, scale = self.resolve_parameters(samples)
        return stats.uniform.cdf(s, loc=location, scale=scale)

class PoissonDistribution(Distribution):
    """
    Poisson based distribution
    """
    def __init__(self, params: dict):
        # TODO: set a formal list of parameter resolving
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)

    def resolve_parameters(self, samples: list) -> float:
        """
        Distribution based function for parameter resolving
        :param samples: list of parent nodes' samples
        :return: location
        """
        location = self.params['loc']
        if callable(self.params['loc']):
            location = self.params['loc'](samples)

        return location

    def icdf(self, r, samples: list) -> float:
        """
        Wrapper for SciPy's poisson icdf function
        :param r: uniformly random sample
        :param samples: list of parent nodes' samples
        :return: sample value
        """
        location = self.resolve_parameters(samples)
        return stats.poisson.ppf(r, mu=location)

    def pdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's poisson pdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: pdf value
        """
        location = self.resolve_parameters(samples)
        return stats.poisson.pdf(s, mu=location)

    def cdf(self, s, samples: list):
        """
        Wrapper for SciPy's poisson cdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: cdf value
        """
        location = self.resolve_parameters(samples)
        return stats.poisson.cdf(s, mu=location)

class TruncatedExponentialDistribution(Distribution):
    """
    Normal based distribution
    """
    def __init__(self, params: dict):
        super().__init__(icdf=self.icdf, pdf=self.pdf, cdf=self.cdf, params=params)

    def resolve_parameters(self, samples: list) -> tuple:
        """
        Distribution based function for parameter resolving
        :param samples: list of parent nodes' samples
        :return: tuple: lower bound a, upper bound b, location and scale
        """

        b = self.params['b']
        if callable(self.params['b']):
            b = self.params['b'](samples)

        location = self.params['loc']
        if callable(self.params['loc']):
            location = self.params['loc'](samples)

        scale = self.params['scale']
        if callable(self.params['scale']):
            scale = self.params['scale'](samples)

        return b, location, scale

    def icdf(self, r, samples: list) -> float:
        """
        Wrapper for SciPy's truncexpon icdf function
        :param r: uniformly random sample
        :param samples: list of parent nodes' samples
        :return: sample value
        """
        b, location, scale = self.resolve_parameters(samples)

        return stats.truncexpon.ppf(r, b=b, loc=location, scale=scale)

    def pdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's truncexpon pdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: pdf value
        """
        b, location, scale = self.resolve_parameters(samples)
        return stats.truncexpon.pdf(s, b=b, loc=location, scale=scale)

    def cdf(self, s, samples: list) -> float:
        """
        Wrapper for SciPy's truncexpon cdf function
        :param s: sample value
        :param samples: list of parent nodes' samples
        :return: cdf value
        """
        b, location, scale = self.resolve_parameters(samples)
        return stats.truncexpon.cdf(s, b=b, loc=location, scale=scale)
