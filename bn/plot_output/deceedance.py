import os
import numpy as np
from matplotlib import pyplot as plt
from scipy import stats as stats

def getDeceedanceProbabilities(intSampAllRuns,LSFnode,maxlevel,intermediatePrF)-> tuple:
    intPerSimLev = 9

    decProb = [1]
    nRuns = len(intSampAllRuns)

    for i in range(0,maxlevel):
        decProb += np.linspace(0.9*10**(-i),intermediatePrF*10**(-i),intPerSimLev).tolist()
    
    decThresh = []
    for iRun in range(0,nRuns):
        CurSamples = []
        for iSam in range(0,len(intSampAllRuns[iRun][0])):
            CurSamples.append(intSampAllRuns[iRun][0][iSam][LSFnode])
        temp = [np.max(CurSamples)]
        for i in range(0,maxlevel):
            CurSamples = []            
            for iSam in range(0,len(intSampAllRuns[iRun][i])):
                CurSamples.append(intSampAllRuns[iRun][i][iSam][LSFnode])            
            temp += np.percentile(CurSamples,np.linspace(90,10,intPerSimLev)).tolist()
        decThresh.append(temp[:])

    

    # print(decThresh)
    # print(type(decThresh))
    decThresh_mean = np.mean(decThresh,axis=0)
    decThresh_cov = np.divide(np.std(decThresh,axis=0),np.mean(decThresh,axis=0))
    
    return (decProb, decThresh_mean.tolist(), decThresh_cov.tolist())

def plotDeceedanceProbabilities(Cases,decProb,decThresh_mean,decThresh_cov,filename,MCSreferenceSolution=None,figsize = [3,2],fontsize = 10,linewidth = 0.5):


    fig = plt.figure(num=None, figsize=figsize, dpi=600, facecolor='w', edgecolor='k')
    fig.set_size_inches(figsize)
    ax = fig.add_subplot(111)
    ax.spines['top'].set_linewidth(0.25)
    ax.spines['right'].set_linewidth(0.25)
    ax.spines['bottom'].set_linewidth(0.25)
    ax.spines['left'].set_linewidth(0.25) 
    ax.tick_params(axis = 'x',labelsize = fontsize-3)
    ax.tick_params(axis = 'y',labelsize = fontsize-3)
    if len(Cases) <= 1:
        plt.plot(decThresh_mean,decProb,'r',label=Cases[0],linewidth=linewidth)        
    else:    
        for iCase in range(0,len(Cases)):
            plt.plot(decThresh_mean[iCase],decProb,'rbgkm'[iCase],label=Cases[iCase],linewidth=linewidth)                    

    if MCSreferenceSolution:
        #Credible intervals
        a_prior = 1
        b_prior = 1
        a_posterior = a_prior + np.array(decProb)*len(MCSreferenceSolution)       
        b_posterior = b_prior + (1-np.array(decProb))*len(MCSreferenceSolution)        
        x = np.percentile(MCSreferenceSolution,np.array(decProb)*100)
        for ix in range(0,len(x)):
            if x[ix] > 1.0:
                x[ix] = 1.0
        y1 = stats.beta.ppf(0.05,a_posterior,b_posterior)
        y2 = stats.beta.ppf(0.95,a_posterior,b_posterior)
        plt.plot(x,decProb,label = 'MCS 1e6 samples',linewidth=linewidth,color = '#5C5C5C'	)
        plt.fill_between(x,y1,y2,facecolor= '#E8E8E8', alpha=0.3)
        
    leg = ax.legend(fontsize =fontsize -3, loc = 0)
    leg.get_frame().set_linewidth(0.25)
    plt.yscale('log')
    plt.xlabel('E[Flow capacity]',fontsize=fontsize)
    plt.ylabel('CDF',fontsize=fontsize)
    plt.grid(True)
    # plt.show()
    os.chdir('/Users/kilianzwirglmaier/Documents')
    fig.savefig(filename+str('_mean.eps'), bbox_inches='tight')

    fig1 = plt.figure(num=None, figsize=figsize, dpi=600, facecolor='w', edgecolor='k')
    fig1.set_size_inches(figsize)
    ax1 = fig1.add_subplot(111)
    ax1.spines['top'].set_linewidth(0.25)
    ax1.spines['right'].set_linewidth(0.25)
    ax1.spines['bottom'].set_linewidth(0.25)
    ax1.spines['left'].set_linewidth(0.25) 
    ax1.tick_params(axis = 'x',labelsize = fontsize-3)
    ax1.tick_params(axis = 'y',labelsize = fontsize-3)
    if len(Cases) <= 1:
        plt.plot(decThresh_cov,decProb,'r',label=Cases[0],linewidth=linewidth)        
    else:    
        for iCase in range(0,len(Cases)):
            plt.plot(decThresh_cov[iCase],decProb,'rbgkm'[iCase],label=Cases[iCase],linewidth=linewidth)                    
    
    leg1 = ax1.legend(fontsize =fontsize -3, loc = 0)
    leg1.get_frame().set_linewidth(0.25)
    plt.yscale('log')
    plt.xlabel('C.O.V.(Flow capacity)',fontsize=fontsize)
    plt.ylabel('Deceedance probability',fontsize=fontsize)
    plt.grid(True)
    # plt.show()
    os.chdir('/Users/kilianzwirglmaier/Documents')
    fig1.savefig(filename+str('_cov.eps'), bbox_inches='tight')

