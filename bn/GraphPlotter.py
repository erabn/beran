##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität München)          #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Hugo Rosero (hugo.rosero@tum.de)
# September 2017

from bn import Network
import numpy as np
import matplotlib.pyplot as plt
#import plotly.plotly as py
from plotly import __version__
from plotly.graph_objs import *
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import plotly
plotly.offline.init_notebook_mode()
from matplotlib.backends.backend_pdf import PdfPages
import networkx as nx

class GraphPlotter(object):
    def __init__(self, network: Network):
        self.network = network
        self.network.filldictionary()
        self.network.findchildren()
        self.nameList=self.getNodeNamesList()

    def getNodeNamesList(self):
        NameList=[]
        for inode in self.network.nodes:
            NameList.append(inode.name)
        return NameList
    
    def getNodeNamesSet(self):
        NameSet={}
        i=0;
        for inode in self.network.nodes:
            NameSet[i]=r''+inode.name
            i=i+1
        return NameSet
    
#    def makeEdges(self):
#        SourceNode=[]
#        TargetNode=[]
#        for inode in self.network.nodes:
#            for jnode in inode.parents:
#                keyj=list(jnode.keys())
#                SourceNode.append(self.nameList.index(keyj[0]))
#                TargetNode.append(self.nameList.index(inode.name))
#        return SourceNode,TargetNode
       
    def createGraph(self): 
#        Source,Target=self.makeEdges()
#        data={
#                "data": [
#                        {
#                                "type": "sankey",
#                                "domain": {"x": [0,1],"y": [0,1] },
#                                "orientation": "v",
#                                #"valueformat": ".0f",
#                                #"valuesuffix": "TWh",
#                                "node": {
#                                        #"pad": 15,
#                                        "thickness": 15,
#                                        "line": {"color": "black", "width": 0.1},
#                                        "label": self.nameList ,
#                                        },
#                                "link": {
#                                        "source": Source,
#                                        "target": Target,
#                                        "value": [1]*len(Source),
#                                        }
#                            }],
#
#            }
#        return data
        DG=nx.DiGraph()
        for inode in self.network.nodes:
            for jnode in inode.parents:
                try:
                    keyj=list(jnode.keys()) 
                    DG.add_edge(keyj[0],inode.name)
                except:
                    keyj=str(jnode)  
                    DG.add_edge(keyj,inode.name)
        return DG
        
    
    def plotGraph(self,filename='MyGraph',save=False):
        G=self.createGraph()
        pos=nx.spring_layout(G)# positions for all nodes
        # nodes
        nx.draw_networkx_nodes(G,pos,
                       node_color='b',
                       node_size=500,
                   alpha=0.8)
        # edges
        nx.draw_networkx_edges(G,pos,width=1.0,alpha=0.5,arrows=True)
        nx.draw_networkx_labels(G,pos,font_size=12)
        plt.title('Graph network\n'+filename,fontsize=20)
        if save==True:
            fn=filename+'.pdf'
            pp = PdfPages(fn)
            pp.savefig()
            pp.close()
        #nx.draw(G)
#        pos=nx.get_node_attributes(G,'pos')
#
#        dmin=1
#        ncenter=0
#        for n in pos:
#            x,y=pos[n]
#            d=(x-0.5)**2+(y-0.5)**2
#            if d<dmin:
#                ncenter=n
#                dmin=d
#
#        p=nx.single_source_shortest_path_length(G,ncenter)
#        
#        edge_trace = Scatter(x=[],y=[],
#                             line=Line(width=0.5,color='#888'),
#                             hoverinfo='none',
#                             mode='lines')
#
#        for edge in G.edges():
#            x0, y0 = G.node[edge[0]]['pos']
#            x1, y1 = G.node[edge[1]]['pos']
#            edge_trace['x'] += [x0, x1, None]
#            edge_trace['y'] += [y0, y1, None]
#
#        node_trace = Scatter(x=[], y=[],text=[],mode='markers',
#                             hoverinfo='text'
#                             )
#
#        for node in G.nodes():
#            x, y = G.node[node]['pos']
#            node_trace['x'].append(x)
#            node_trace['y'].append(y)
#        
#        fig = Figure(data=Data([edge_trace, node_trace]))
#
#        plot(fig, filename='networkx')
        
#        data_trace = dict(
#                type='sankey',
#                width = 1118,
#                height = 772,
#                domain = dict(x =  [0,1],y =  [0,1]),
#                orientation = "v",
#                #valueformat = ".0f",
#                #valuesuffix = "TWh",
#                node = dict(
#                        #pad = 15,
#                        thickness = 15,
#                        line = dict(color = "black",width = 0.1),
#                        label =  data['data'][0]['node']['label']
#                        ),
#                link = dict(
#                        source =  data['data'][0]['link']['source'],
#                        target =  data['data'][0]['link']['target'],
#                        value =  data['data'][0]['link']['value']
#                        ))
#        fig = dict(data=[data_trace])
#        plot(fig, validate=False)