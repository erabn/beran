##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität Münche)           #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017

from bn import Network
from bn.Sampling.Sampling import Sampling


class LikelihoodWeighting(Sampling):
    def __init__(self, network: Network):
        super().__init__(network=network)

    def sample(self, amount: int) -> tuple:
        """
        Samples and weighs a network under given parameters
        :return: tuple: (samples, weight)
        """
        samples = {}
        weights = []
        for node in self.network.nodes:
            samples[node.name]=[]

        for i in range(0, amount):
            if (i+1)%100 == 0:
                print(i+1)            
            sample = {}
            weight = 1
            for node in self.network.nodes:
                sample[node.name] = node.fwdsample(sample)
                weight *= node.weight(sample)
            self.AddSample(samples,sample)
            weights.append(weight)
        return samples, weights
    
    def AddSample(self,samples,sample):
        for inode in self.network.nodes:
            samples[inode.name].append(sample[inode.name])
