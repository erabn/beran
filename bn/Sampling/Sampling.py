##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität Münche)           #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017

from bn import Network
import numpy as np


class Sampling(object):
    def __init__(self, network: Network):
        self.network = network
        ## Use a random state seed to be consistent and initiated locally
        #self.random = np.random.RandomState(seed)

    def sample(self):
        raise NotImplementedError
