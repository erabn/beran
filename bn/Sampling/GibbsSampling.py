##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität München)          #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017


from bn import Network
from bn.Sampling.Sampling import Sampling
import random

class GibbsSampler(Sampling):
    def __init__(self, network: Network):
        super().__init__(network=network)

        self.network.filldictionary()
        self.network.findchildren()             



    def sample(self, amount, burnin =  'automatic', seed = None, LSFcondition = False,AccPrOnFailureSurface = None,nchains = 1):
        """
        Samples and a network under given parameters
        :return: tuple: (samples)
        LSFcondition: check if sample[LSFnodename] is smaller than gp0 -- if yes accept sample else reject
        if each chain has the same length amount can be a scalar otherwise a list
        """

        if burnin == 'automatic':
            burnin = 500
        dic = self.network.dictionary


        # objects that need to be sampled/evaluated (en block) when Gibbs sampling 
        self.network.findGibbsSamplingObj()            

        # find barren nodes and establish the PMF/PDF of each node conditional on its Markov blanket
        if LSFcondition: # sample as if the LSF node itself was unbarren but all its descendants were barren
            self.network.nodes[self.network.dictionary[LSFcondition['LSFnodename']]].evidence = True
            self.network.isbarren()
            self.network.nodes[self.network.dictionary[LSFcondition['LSFnodename']]].evidence = None
            self.network.nodes[self.network.dictionary[LSFcondition['LSFnodename']]].barren = 'barren' 
        else:
            self.network.isbarren()

        # Get for each DistributionNode or discreteCPTNode the conditional PDF/PMF 
        for inode in self.network.nodes:
            idx = self.network.dictionary[inode.name]
            if inode.barren == 'unbarren':
                if inode.__class__.__name__ == 'DistributionNode':
                    self.network.nodes[idx].condpdfprop = self.network.makecondpdfprop(inode.name)
                elif inode.__class__.__name__ == 'DiscreteCPTNode':
                    self.network.nodes[idx].condpmf = self.network.makecondpmf(inode.name)

        #samples = []
        samples={}
        for inode in self.network.nodes:
            samples[inode.name]=[]
        previous_sample = {}

        # Sample
        for ichain in range(0,nchains):

            # obtain the initiating sample of each chain through forward sampling
            sample = {}
            if seed == None:                  
                for inode in self.network.nodes:
                    if inode.evidence:
                        sample[inode.name] = inode.evidence_sample(sample)
                    else:
                        sample[inode.name] = inode.fwdsample(sample)
            else:
                for inode in self.network.nodes:
                    if inode.name in seed.keys():
                        sample[inode.name] = seed[inode.name][ichain]
                    else:
                        if inode.evidence:
                            sample[inode.name] = inode.evidence_sample(sample)
                        else:
                            sample[inode.name] = inode.fwdsample(sample)                
                
            if burnin:
                print('Burn-in phase...')
            for i in range(0,burnin):
                if ((ichain * burnin+i+1) %100) == 0:
                    print(i+1)                

                for iSampObj in self.network.ToBeSampled:
                    if iSampObj.barren == 'barren':
                        sample[iSampObj.name] = iSampObj.fwdsample(sample) 
                    elif iSampObj.__class__.__name__ == 'list': # Block sample
                        #TODO
                        pass
                    elif iSampObj.__class__.__name__ == 'FunctionNode':
                        sample[iSampObj.name] = iSampObj.fwdsample(sample)
                    elif iSampObj.__class__.__name__ in ['DistributionNode','DiscreteCPTNode']:
                        if iSampObj.evidence:
                            if iSampObj.evidence.type == '=':
                                sample[iSampObj.name] = iSampObj.evidence.value                                 
                        else:
                            sample[iSampObj.name] = iSampObj.SCgibbssample(sample) 
            
            #samples.append(sample.copy())
            self.AddSample(samples,sample.copy())
            previous_sample = sample.copy()            

            if type(amount) == list:
                amount_chain = amount[ichain]
                if ichain == 0:
                    print('Sampling phase...')                
            else:
                amount_chain = amount
                print('Sampling phase...')   

            for i in range(1,amount_chain):                    
                if ichain == 0:
                    # print(i+1)
                    if ((i+1)%100) == 0:
                        print(i+1)
                else:
                    # print(sum(amount[:ichain])+ i+1)
                    if ((sum(amount[:ichain])+ i+1)%100) == 0:
                        print(sum(amount[:ichain])+ i+1) 
                for iSampObj in self.network.ToBeSampled:
                    # print('     '+str(iSampObj.name))
                    if iSampObj.barren == 'barren':
                        sample[iSampObj.name] = iSampObj.fwdsample(sample)
                    elif iSampObj.__class__.__name__ == 'list': # Block sample
                        pass
                    elif iSampObj.__class__.__name__ == 'FunctionNode': # Block sample
                        sample[iSampObj.name] = iSampObj.fwdsample(sample)
                        # print('sample[iSampObj.name]'+str(iSampObj.func(sample)))                     
                    elif iSampObj.__class__.__name__ in ['DistributionNode','DiscreteCPTNode']:
                        if iSampObj.evidence:
                            if iSampObj.evidence.type == '=':
                                sample[iSampObj.name] = iSampObj.evidence.value 
                        else:
                            sample[iSampObj.name] = iSampObj.SCgibbssample(sample)                                  
                                    
                if LSFcondition: # if the sampling is conditional on a threshold given for a LSF node

                    if all(self.network.nodes[dic[iBRV]].__class__.__name__ == 'DiscreteCPTNode' for iBRV in self.network.nodes[dic[LSFcondition['LSFnodename']]].parents):
                    # all basic random variables are discrete and the intermediate failure probability should be constant p0

                        if sample[LSFcondition['LSFnodename']] < LSFcondition['gp0']: # accept if g(x) < LSFcondition                        
                            #samples.append(sample.copy()) # accept the sample 
                            self.AddSample(samples,sample.copy()) #accept the sample
                            for iSampObj in self.network.ToBeSampled:
                                if iSampObj.__class__.__name__ in ['DistributionNode', 'DiscreteCPTNode']: 
                                    if iSampObj.univariatesampler == 'MH':
                                        if iSampObj.adaptive:                                    
                                            iSampObj.AdaptionAcceptedSamples += iSampObj.AdaptionAccept1stStage
                                            iSampObj.AdaptionTotalSamples  += 1

                        elif sample[LSFcondition['LSFnodename']] == LSFcondition['gp0']: 
                        # accept with probability ... if g(x) = LSFcondition                        
                            
                            r = random.random()
                            if r <= AccPrOnFailureSurface:                                
                                #samples.append(sample.copy()) # accept the sample 
                                self.AddSample(samples,sample.copy()) #accept the sample
                                for iSampObj in self.network.ToBeSampled:
                                    if iSampObj.__class__.__name__ in ['DistributionNode', 'DiscreteCPTNode']:  
                                        if iSampObj.univariatesampler == 'MH':
                                            if iSampObj.adaptive:                                    
                                                iSampObj.AdaptionAcceptedSamples += iSampObj.AdaptionAccept1stStage
                                                iSampObj.AdaptionTotalSamples  += 1
                            else:
                                #samples.append(previous_sample.copy())
                                self.AddSample(samples,previous_sample.copy()) 
                                sample = previous_sample.copy()
                                for iSampObj in self.network.ToBeSampled:
                                    if iSampObj.__class__.__name__ in ['DistributionNode', 'DiscreteCPTNode']:                                      
                                        if iSampObj.univariatesampler == 'MH':
                                            if iSampObj.adaptive:
                                                iSampObj.AdaptionTotalSamples  += 1
                        else: #repeat the previous sample
                            #samples.append(previous_sample.copy())
                            self.AddSample(samples,previous_sample.copy()) #accept the sample
                            sample = previous_sample.copy()
                            for iSampObj in self.network.ToBeSampled:
                                if iSampObj.__class__.__name__ in ['DistributionNode', 'DiscreteCPTNode']:  
                                    if iSampObj.univariatesampler == 'MH':
                                        if iSampObj.adaptive:
                                            iSampObj.AdaptionTotalSamples  += 1
                    else:
                        if sample[LSFcondition['LSFnodename']] <= LSFcondition['gp0']: # check whether sample is not in the intermediate failure domain                        
                            #samples.append(sample.copy()) # accept the sample  
                            self.AddSample(samples,sample.copy()) #accept the sample
                            for iSampObj in self.network.ToBeSampled:
                                if iSampObj.__class__.__name__ in ['DistributionNode', 'DiscreteCPTNode']:  
                                    if iSampObj.univariatesampler == 'MH':
                                        if iSampObj.adaptive:                                    
                                            iSampObj.AdaptionAcceptedSamples += iSampObj.AdaptionAccept1stStage
                                            iSampObj.AdaptionTotalSamples  += 1
                    
                        else:
                            #samples.append(previous_sample.copy())
                            self.AddSample(samples,previous_sample.copy()) #accept the sample
                            sample = previous_sample.copy()
                            for iSampObj in self.network.ToBeSampled:
                                if iSampObj.__class__.__name__ in ['DistributionNode', 'DiscreteCPTNode']: 
                                    if iSampObj.univariatesampler == 'MH':
                                        if iSampObj.adaptive:
                                            iSampObj.AdaptionTotalSamples  += 1
                                            
                else:
                    #samples.append(sample.copy())
                    self.AddSample(samples,sample.copy()) #accept the sample
                previous_sample = sample.copy()

            # Adapt MH proposal PDF
            self.network.adaptMHproposal(ichain)                
                    
        return samples
    def AddSample(self,samples,sample):
        for inode in self.network.nodes:
            samples[inode.name].append(sample[inode.name])