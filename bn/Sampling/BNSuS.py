##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität München)          #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017


import numpy as np
from bn import Network
from bn import Evidence
from bn.Sampling.Sampling import Sampling
from bn.Sampling.GibbsSampling import GibbsSampler

class BNSuS(Sampling):
    def __init__(self, network: Network):
        super().__init__(network=network)   


    def getSeeds(self, samples,LSFnode ,nsampperlevel,p0)-> tuple:

        LSFsamples = []
        for i in range(0,nsampperlevel):
            LSFsamples.append(samples[LSFnode][i])
        LSFsortindices = np.argsort(LSFsamples)
        gp0 = np.percentile(LSFsamples,p0*100,interpolation='midpoint')

        if gp0 > 0: 
            n_seeds = round(nsampperlevel * p0)
            PrF_int = p0
        else:
            n_seeds = sum(1 for i in LSFsamples if i <= 0)
            PrF_int = n_seeds/nsampperlevel
    
        LSFsortindices = LSFsortindices[0:n_seeds]
#        np.random.shuffle(LSFsortindices)

        seeds = {}
        for inode in samples.keys():
            seeds[inode]=[]
            for i in range(0,n_seeds):
                seeds[inode].append(samples[inode][LSFsortindices[i]])
        
        #for i in range(0,n_seeds):
            #seeds.append(samples[LSFsortindices[i]])  

        nOnSurf = sum(1 for i in LSFsamples if i == gp0) # number of samples on the LS surface         
        if nOnSurf:
            #AccPrOnFailureSurface = sum(1 for i in seeds if i[LSFnode] == gp0)/sum(1 for i in LSFsamples if i == gp0)
            AccPrOnFailureSurface = sum(1 for i in seeds[LSFnode] if i == gp0)/sum(1 for i in LSFsamples if i == gp0)
        else:
            AccPrOnFailureSurface = 1

        return gp0, PrF_int, n_seeds, seeds, AccPrOnFailureSurface

    def sample(self, LSFnode ,nsampperlevel: int,nsampfinalstep = 'automatic',burnin = 'automatic', intermediatePrF = 0.1, maxlevel = 20,CDFplot = False,seed=None):
        """
        Samples and a network under given parameters
        :return: tuple: (samples)
        """

        # clasical failure probability --> CDFplot = False
        # deceedance-probability-plot --> CDFplot = True
        if CDFplot:
            failureThreshold = float('-inf')
        else:
            failureThreshold = 0            
        self.network.filldictionary()
        dic = self.network.dictionary

        # LSF node cannot have evidence
        if self.network.nodes[dic[LSFnode]].evidence:
            self.network.nodes[dic[LSFnode]].evidence = False 
            print('ERROR: LSFnode cannot have evidence')

        # Burnin
        if burnin == 'automatic':
            burnin = 500
        if nsampfinalstep == 'automatic':
            nsampfinalstep = 0


        p0 = intermediatePrF #intermediate failure probability
        gp0, PrF_int, n_seeds, seeds = ([] for i in range(4))

        # Make Gibbs Object
        GibbsObj = GibbsSampler(self.network)
        
        # First unconditional level 
        intermediatesamples = []
        intermediatesamples.append(GibbsObj.sample(amount = nsampperlevel, burnin = burnin,seed=seed))
        temp_gp0, temp_PrF_int, n_seeds, temp_seeds, AccPrOnFailureSurface = self.getSeeds(intermediatesamples[0],LSFnode ,nsampperlevel,p0)
        gp0.append(temp_gp0)
        PrF_int.append(temp_PrF_int)
        seeds.append(temp_seeds)
        print('Intermediate Threshold: ',temp_gp0)

        ## TODO automatically determine if LSF evaluations are costly
        if not hasattr(self.network.nodes[dic[LSFnode]],'costly'):
            self.network.nodes[dic[LSFnode]].costly = False
            print('LSF is uncostly')

        # Use MH for basic random variables of costly LSFs
        if self.network.nodes[dic[LSFnode]].costly:
            print('LSF is costly')
            for iparent in self.network.nodes[dic[LSFnode]].parents:
                self.network.nodes[dic[iparent]].univariatesampler = 'MH'
                self.network.setGibbsSampler(self.network.nodes[dic[iparent]])                    

        # Use MH for basic random variables of bindary/multinomial basic random variables                
        for iparent in self.network.nodes[dic[LSFnode]].parents:
            if self.network.nodes[dic[iparent]].__class__.__name__ == 'DiscreteCPTNode':
                self.network.nodes[dic[iparent]].univariatesampler = 'MH'
                self.network.setGibbsSampler(self.network.nodes[dic[iparent]])

        # Conditional steps        
        count = 1
        while 1:
            count += 1

            if gp0[-1] <= failureThreshold:
                PrF = np.prod(PrF_int)
                break
            elif count>maxlevel:
                print('Maximum number of Subset simulation levels reached: Pr(F) < ', np.prod(PrF_int))
                PrF = '<' + str(np.prod(PrF_int))
                break

            # Determine the length of each chain
            nchainlen = [int(np.floor(nsampperlevel/n_seeds))]*n_seeds
            nchainlen[:nsampperlevel%n_seeds] = [x+1 for x in nchainlen[:nsampperlevel%n_seeds]] 
              
            # If LSF evaluation is costly or if all basic random variables are discrete do not directly set evidence to the BN but check LSF condition at the end of each sample generated  
            if self.network.nodes[dic[LSFnode]].costly:         
                LSFcondition = {'LSFnodename':LSFnode,'gp0':gp0[-1]}
                intermediatesamples.append(GibbsObj.sample(amount = nchainlen, burnin = 0, seed = seeds[-1], LSFcondition = LSFcondition, nchains = n_seeds))            
            elif all(self.network.nodes[dic[iBRV]].__class__.__name__ == 'DiscreteCPTNode' for iBRV in self.network.nodes[dic[LSFnode]].parents):
                LSFcondition = {'LSFnodename':LSFnode,'gp0':gp0[-1]}
                intermediatesamples.append(GibbsObj.sample(amount = nchainlen, burnin = 0, seed = seeds[-1], LSFcondition = LSFcondition, AccPrOnFailureSurface = AccPrOnFailureSurface, nchains = n_seeds))            
            else:
                self.network.nodes[dic[LSFnode]].evidence = Evidence.Evidence(typ = '<=',val = gp0[-1]) 
                intermediatesamples.append(GibbsObj.sample(amount = nchainlen, burnin = 0, seed = seeds[-1], nchains = n_seeds))

            temp_gp0, temp_PrF_int, n_seeds, temp_seeds, AccPrOnFailureSurface = self.getSeeds(intermediatesamples[-1],LSFnode ,nsampperlevel,p0)

            gp0.append(temp_gp0)
            PrF_int.append(temp_PrF_int)
            seeds.append(temp_seeds)

            print('Intermediate Threshold: ',temp_gp0)
            print('PrF int: ', str(PrF_int))

        # additional conditonal samples
        if nsampfinalstep == 'automatic':
            pass
        elif n_seeds > nsampfinalstep:
            pass
        else:
            pass
        ConditonalSamples = seeds[-1]

        # Change network to original setting
        for inode in self.network.nodes:
            if inode.__class__.__name__ == 'DistributionNode':
                if inode.univariatesampler == 'MH':
                    inode.StdPropPDF = inode.AdaptionInitStdPropPDF
                    inode.AdaptionCount = 0               
                    inode.AdaptionAcceptedSamples = 0
                    inode.AdaptionTotalSamples = 0  
            if inode.__class__.__name__ == 'DiscreteCPTNode':
                if inode.univariatesampler == 'MH':
                    inode.transitionPropProb = inode.AdaptionInitTransitionPropProb
                    inode.AdaptionCount = 0               
                    inode.AdaptionAcceptedSamples = 0
                    inode.AdaptionTotalSamples = 0  

        self.network.nodes[dic[LSFnode]].evidence = False 

        print('p0: ')
        print(PrF_int[-1])

        return PrF, ConditonalSamples, intermediatesamples
