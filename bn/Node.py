##############################################################################################
#--------------------------------------- BeraN ----------------------------------------------#   
##############################################################################################
#  BeraN is a Bayesian network software with a focus on sampling based inference techniques  #
#  for applications in the field of reliability engineering                                  # 
##############################################################################################
# developed in the Engineering Risk analysis group (Technische Universität Münche)           #
#----------------------------------- era.bgu.tum.de -----------------------------------------#
# Coded by Kilian Zwirglmaier (kilian.zwirglmaier@tum.de)
# July 2017

from bn import Distribution
from bn import Evidence
import numpy as np
import random
import copy

class Node(object):
    """
    A single node in a Bayesian network
    """
    def __init__(self, name: str, parents: list, evidence: Evidence = None):
        """
        Constructor
        :param name: readable name of the node
        :param parents: a list of nodes, that point to this node
        :param evid: optional Evidence type object
        """
        self.name = name
        self.parents = parents
        self.evidence = evidence


class DistributionNode(Node):
    """
    Node that uses distributions for sampling
    """
    def __init__(self, name: str, parents: list, distribution: Distribution, evidence: Evidence=None):
        """
        Constructor
        :param name: readable name of the node
        :param parents: a list of nodes, that point to this node
        :param distribution: distribution object (see Distribution.py)
        :param evid: Evidence object (see Evidence.py)
        """
        # Type checks
        if not isinstance(distribution, Distribution.Distribution):
            raise TypeError('Distribution has to inherit Distribution class')

        self.distribution = distribution
        super().__init__(name=name, parents=parents, evidence=evidence)

    def fwdsample(self, sample: list) -> float:
        """
        sample from a node conditional on its parents
        :param sample: a list of sample from parent nodes
        :return: sample value
        """
        if self.evidence:
            my_sample = self.evidence_sample(sample)
        else:
            r = random.random()
            my_sample = self.distribution.icdf(r, sample)
        return float(my_sample)

    def SCgibbssample(self,sample) -> float:
        """
        sample from the node self conditional on its Markov Blanket either using Slice sampler or a univariate Metropolis algorithm
        """
        
        if self.univariatesampler == 'slice':
            """
            Slice sampler
            """
            w = self.typStepWidth 
            niter = 1 # number of iterations of the slice sampler
    
            for iter in range (0,niter):               
                urand = random.random()
                u = np.log(urand) + self.condpdfprop(sample) # log scale                                       
                uprime = random.random()                 
                samplexmin = copy.deepcopy(sample)
                samplexmin[self.name] -= uprime * w
                samplexmax = copy.deepcopy(sample)
                samplexmax[self.name] = samplexmin[self.name] + w
                  
                while u < self.condpdfprop(samplexmin):                                
                    samplexmin[self.name] -= w

                while u < self.condpdfprop(samplexmax):  
                    samplexmax[self.name] += w
                samplexprime = copy.deepcopy(samplexmin)
 
                exit_count = 0
                while 1:     
                    exit_count += 1
                    uprime = random.random()
                    samplexprime[self.name] = samplexmin[self.name] + uprime * (samplexmax[self.name]-samplexmin[self.name])
                    if u < self.condpdfprop(samplexprime):
                        my_sample = samplexprime[self.name]
                        break
                    elif exit_count > 1e4:
                        my_sample = samplexprime[self.name]  
                        print('Warning slicesampler did not converge for node '+str(self.name))
                        print ('sample')
                        print(str(sample))
                        break 
                    if samplexprime[self.name] < sample[self.name]:                          
                        samplexmin[self.name] = samplexprime[self.name]
                    else:
                        samplexmax[self.name] = samplexprime[self.name]

        elif self.univariatesampler == 'MH':
            """
            Univariate Metropolis with normal proposal
            """
            proposalStd = self.StdPropPDF 
            log_fold = self.condpdfprop(sample)
            sample_cand = random.normalvariate(sample[self.name],proposalStd)
            samplecopy = copy.deepcopy(sample)
            samplecopy[self.name] = sample_cand
            accrate = min(1,np.exp(self.condpdfprop(samplecopy)-log_fold))
            if accrate >= random.random(): 
                my_sample = sample_cand
                self.AdaptionAccept1stStage = 1 
            else: #candidate is not accepted use old state
                my_sample = sample[self.name]
                self.AdaptionAccept1stStage = 0   
        return float(my_sample)

    def weight(self, sample: list) -> float:
        """
        Calculates weight given parent sample
        :param sample: parent nodes' sample
        :return: weight value
        """
        my_weight = 1
        if self.evidence:
            my_weight = self.evidence_weight(sample)
        return float(my_weight)

    def evidence_sample(self, sample: list) -> float:
        """
        Sampling with evidence
        :param sample: parent nodes' sample
        :return: sample value
        """
        t = self.evidence.type
        if t == '=':
            return self.evidence.value
        elif t == '<' or t == '<=':
            r = np.random.uniform(low = 0,high = self.distribution.cdf(self.evidence.value,sample))
            return self.distribution.icdf(r, sample)
        elif t == '>' or t =='>=':
            r = np.random.uniform(low = self.distribution.cdf(self.evidence.value,sample),high = 1)
            return self.distribution.icdf(r, sample)
        elif t == 'between':
            r = np.random.uniform(low = self.distribution.cdf(self.evidence.value[0],sample),high = self.distribution.cdf(self.evidence.value[1],sample))
            return self.distribution.icdf(r, sample)
        else:
            raise ValueError('Evidence type not supported')

    def evidence_weight(self, sample: list) -> float:
        """
        Weighting with evidence
        :param sample: parent nodes' sample
        :return: weight value
        """
        t = self.evidence.type
        if t == '=':
            return self.distribution.pdf(self.evidence.value, sample)
        elif t == '<' or t == '<=':
            my_weight = self.distribution.cdf(self.evidence.value, sample)
            return my_weight
        elif t == '>' or t == '>=':
            my_weight = 1-self.distribution.cdf(self.evidence.value, sample)
            return my_weight
        elif t == 'between':
            my_weight = self.distribution.cdf(self.evidence.value[1], sample) - self.distribution.cdf(self.evidence.value[0], sample)
            return my_weight
        else:
            raise ValueError('Evidence type not supported')

    def adaptUnivariateMHproposal(self):
        if self.adaptive:
            self.AdaptionCount += 1            
            gamma =  self.AdaptionCount**(-0.5)
            accmean = self.AdaptionAcceptedSamples/self.AdaptionTotalSamples
            self.StdPropPDF = np.exp(np.log(self.StdPropPDF)+gamma * (accmean - 0.44))
            self.AdaptionAcceptedSamples = 0
            self.AdaptionTotalSamples = 0 
  
class FunctionNode(Node):
    """
    Node that uses functions for sampling
    """
    def __init__(self, name: str, parents: list, func, evidence: Evidence=None):
        """
        Constructor
        :param name: readable name of the node
        :param parents: a list of nodes, that are descendants of this one
        :param func: Function that accepts only 1 parameter - list of sample
        """
        # Type checks
        if not callable(func):
            raise TypeError('func has to be a callable function')

        self.func = func
        super().__init__(name=name, parents=parents, evidence=evidence)

    def fwdsample(self, sample: list) -> float:
        """
        Uses predefined function to sample this node using parent node sample
        :param sample: prior to this node sample (of parent nodes and more)
        :return: single sample for this function node
        """
        if self.evidence:
            if self.evidence.type == '=':
                my_sample = self.evidence.value
            else:
                my_sample = self.func(sample)    
        else:
            my_sample = self.func(sample)     
        return float(my_sample)

    def weight(self, sample: list) -> float:
        """
        Calculates weight of a function node
        :param sample: prior to this node sample (of parent nodes and more)
        :return: weight (in range [0..1]) of the node
        """

        if self.evidence:
            my_weight = 0
            curValue = sample[self.name]
            if self.evidence.type == '=':
                if curValue == self.value:
                    my_weight = 1                
            elif self.evidence.type == '<':
                if curValue < self.value:
                    my_weight = 1
            elif self.evidence.type == '>':
                if curValue > self.value:
                    my_weight = 1
            elif self.evidence.type == '<=':
                if curValue <= self.value:
                    my_weight = 1
            elif self.evidence.type == '>=':
                if curValue >= self.value:
                    my_weight = 1
            elif self.evidence.type == 'between':
                if curValue > self.evidence.value[0] and curValue < self.evidence.value[1]:
                    my_weight = 1
        else:
            my_weight = 1.0
        return my_weight


class DiscreteCPTNode(DistributionNode):
    """
    Node that uses conditional probability tables for returning sample
    """
    def __init__(self, name: str, parents: list, cpt: list, states: list, evidence: Evidence=None,distribution: Distribution=None):
        """
        Node that uses conditional probability tables for sampling
        :param name: readable name of the node
        :param parents: a list of nodes, that are parents of this one
        :param cpt: conditional probability table in a form of matrix
        :param states: possible sample value list
        """
        distribution = Distribution.CPTDistribution(cpt=cpt, states=states, parents=parents)
        super().__init__(name=name, parents=parents,distribution = distribution, evidence=evidence)


    def getweight(self,sample):
        # The probability of self.evidence conditional on the current state of the parents
        my_weight = self.distribution.pdf(sample)
        return float(my_weight)

    def fwdsample(self,sample):
        # Generate a sample from the pmf of the node self conditional on the current state of the parents
        r = random.random()           
        my_sample = self.distribution.icdf(r,sample)
        return my_sample

    def SCgibbssample(self,sample):        
        if self.univariatesampler == 'MH':
            """
            Discrete, univariate Metropolis with transition probability
            """
            print('ERROR: Metropolis Hastings sampler for discrete node is not implemented')
            pass
        else:        
            r = random.random()
            count = 0
            cmf_temp = 0
            pmf = self.condpmf(sample)            
            while 1:
                cmf_temp += pmf[count]
                if r <= cmf_temp:
                    my_sample = self.distribution.params['states'][count]
                    break
                count += 1
        return my_sample


    def adaptUnivariateMHproposal(self): ## addaption rule should be reconsidered
        pass
        #TODO implement adaption rule for discrete Metropolis Hastings algorithm
#        if self.adaptive:
#            self.AdaptionCount += 1 
#            gamma =  self.AdaptionCount**(-0.25)
#            accmean = self.AdaptionAcceptedSamples/self.AdaptionTotalSamples
#            print(self.name)
#            print(' mean acceptance rate: ' + str(accmean))
#            self.transitionPropProb = min(1,np.exp(np.log(self.transitionPropProb)+gamma * (accmean - 0.44)))
#            print(' transition proposal proability: ' + str(self.transitionPropProb))            
#            self.AdaptionAcceptedSamples = 0
#            self.AdaptionTotalSamples = 0  
