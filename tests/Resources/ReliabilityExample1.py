from bn.Network import Network
from bn import Node
from bn import Evidence
from bn import Distribution
import numpy as np

net = Network()

# Common parents of basic rvs
name = 'X0'
params = {'loc': 0, 'scale': 1}
parents = []
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode = []

# BasicRVs
name = 'BRV'+ str(1)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(1)
params = {'loc': lambda nodes: nodes['BRV'+ str(1)], 'scale': 1}
parents = ['BRV'+ str(1)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)

# BasicRVs
name = 'BRV'+ str(2)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(2)
params = {'loc': lambda nodes: nodes['BRV'+ str(2)], 'scale': 1}
parents = ['BRV'+ str(2)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)


# BasicRVs
name = 'BRV'+ str(3)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(3)
params = {'loc': lambda nodes: nodes['BRV'+ str(3)], 'scale': 1}
parents = ['BRV'+ str(3)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)


# BasicRVs
name = 'BRV'+ str(4)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(4)
params = {'loc': lambda nodes: nodes['BRV'+ str(4)], 'scale': 1}
parents = ['BRV'+ str(4)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)


# BasicRVs
name = 'BRV'+ str(5)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(5)
params = {'loc': lambda nodes: nodes['BRV'+ str(5)], 'scale': 1}
parents = ['BRV'+ str(5)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)


# BasicRVs
name = 'BRV'+ str(6)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(6)
params = {'loc': lambda nodes: nodes['BRV'+ str(6)], 'scale': 1}
parents = ['BRV'+ str(6)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)



# BasicRVs
name = 'BRV'+ str(7)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(7)
params = {'loc': lambda nodes: nodes['BRV'+ str(7)], 'scale': 1}
parents = ['BRV'+ str(7)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)


# BasicRVs
name = 'BRV'+ str(8)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(8)
params = {'loc': lambda nodes: nodes['BRV'+ str(8)], 'scale': 1}
parents = ['BRV'+ str(8)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)

# BasicRVs
name = 'BRV'+ str(9)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(9)
params = {'loc': lambda nodes: nodes['BRV'+ str(9)], 'scale': 1}
parents = ['BRV'+ str(9)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)

# BasicRVs
name = 'BRV'+ str(10)
params = {'loc': lambda nodes: nodes['X0'], 'scale': 1}
parents = ['X0']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

parentsLSFnode.append(name)


# Measurements of BasicRVs
name = 'M_BRV'+ str(10)
params = {'loc': lambda nodes: nodes['BRV'+ str(10)], 'scale': 1}
parents = ['BRV'+ str(10)]
distribution = Distribution.NormalDistribution(params)
evid = Evidence.Evidence(typ = '=', val = 3)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution,evidence = evid)
net.nodes.append(node)



name = 'LSFnode'
parents =  parentsLSFnode
function = lambda nodes: 40 - sum(nodes[i_key] for i_key in parentsLSFnode)
node = Node.FunctionNode(name = name, parents = parents, func = function)
net.nodes.append(node)
