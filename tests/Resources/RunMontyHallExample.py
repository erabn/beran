# Sample from CLGexample1 through Gibbs sampling

import os 
import timeit
os.chdir("/Users/hfrv2/Documents/BERAN")
import tests.Resources.MontyHallExample as Examp
import bn.Sampling.GibbsSampling as Gibbs
import bn.Plotter as plo
import bn.Evidence as Ev

'''
sample from BN and take the time required for sampling
'''
nsamples = 1000
GibbsObj = Gibbs.GibbsSampler(Examp.net)
GibbsObj.network.filldictionary()
d = GibbsObj.network.dictionary
GibbsObj.network.nodes[d['Choice']].evidence = Ev.Evidence('Door1','=')
GibbsObj.network.nodes[d['GoatAppeared']].evidence = Ev.Evidence('Door3','=')

start = timeit.default_timer()
samples = GibbsObj.sample(amount = nsamples, burnin = 500)
# # samples = GibbsObj.sample(amount = 1000, burnin = 100,nchains)
end = timeit.default_timer()
print('time[s]: ',end-start)


# Choice
PrChoice = [0,0,0]
j = 0
for state in Examp.net.nodes[Examp.net.dictionary['Choice']].distribution.params['states']:
    for i in range(0,nsamples):
        #if samples[i]['Choice'] == state:
        if samples['Choice'][i] == state:
            PrChoice[j] += 1/nsamples
    j += 1
    
# Car
PrCar = [0,0,0]
j = 0
for state in Examp.net.nodes[Examp.net.dictionary['Car']].distribution.params['states']:
    for i in range(0,nsamples):
        #if samples[i]['Car'] == state:
        if samples['Car'][i] == state:
            PrCar[j] += 1/nsamples
    j += 1    
    
# GoatAppeared
PrGoatAppeared = [0,0,0]
j = 0
for state in Examp.net.nodes[Examp.net.dictionary['GoatAppeared']].distribution.params['states']:
    for i in range(0,nsamples):
        #if samples[i]['GoatAppeared'] == state:
        if samples['GoatAppeared'][i] == state:
            PrGoatAppeared[j] += 1/nsamples
    j += 1        
    
plotes=plo.Plotter(Examp.net,samples)
#plotes.plotHistCatNode('Car',save=True)
plotes.scatterNodes('Car','GoatAppeared',save=True)