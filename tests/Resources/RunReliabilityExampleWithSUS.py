# Sample from CLGexample1 through Subset simulation with Gibbs sampling

import os 
# import timeit
# import importlib 
# import matplotlib.pyplot as plt 
os.chdir("/Users/hfrv2/Documents/beran")
import tests.Resources.ReliabilityExample1 as Examp
import bn.Sampling.BNSuS as bnsus
import bn.Plotter as plo


'''
Directory of BN input file
'''
SuSObj = bnsus.BNSuS(Examp.net)

Examp.net.filldictionary()
dic = Examp.net.dictionary
Examp.net.nodes[dic['LSFnode']].costly = True

nSimRuns = 1
nsampperlevel = 1000
FailureProbabilities = []
for i in range(0,nSimRuns):

	# start = timeit.default_timer()
	PrF, ConditionalSamples, intermediatesamples = SuSObj.sample(LSFnode = 'LSFnode' ,nsampperlevel  = nsampperlevel, intermediatePrF = 0.1,burnin = 500)
	# end = timeit.default_timer()
	# FailureProbabilities.append(PrF)
	# print('Simulation run: '+str(i) +'; time[s]: ' +str(end-start))
	# print('Pr(F): '+str(PrF))

plotes=plo.Plotter(Examp.net,intermediatesamples[1])
#plotes.histNode('X0')
plotes.scatterNodes('BRV1','BRV2',save=True)