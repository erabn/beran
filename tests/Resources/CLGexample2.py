from bn.Network import Network
from bn import Node
from bn import Evidence
from bn import Distribution

net = Network()

# First node
name = 'X1'
params = {"loc": 0, "scale": 1}
parents = []
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Second node
name = 'X2'
params = {"loc": 0, "scale": 1}
parents = []
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Third node
# name = 'X3'
# params = {"loc": lambda samples: samples['X1']+samples['X2'], "scale": 1}
# parents = ['X1','X2']
# distribution = Distribution.NormalDistribution(params)
# node = Node.DistributionNode(name=name, parents=parents, distr=distribution)
# net.nodes.append(node)

# Function 3,4
name = 'f34'
parents = ['X1','X2']
myfunc = lambda node: node['X1'] + node['X2']
node = Node.FunctionNode(name=name, parents=parents, func=myfunc)
net.nodes.append(node)

# Fourth node
name = 'X4'
params = {"loc": lambda node: node['f34'], "scale": 1}
parents = ['f34']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Function 4,5,a
name = 'f45a'
parents = ['X4']
myfunc = lambda node: node['X4']
node = Node.FunctionNode(name=name, parents=parents, func=myfunc)
net.nodes.append(node)

# Function 4,5,b
name = 'f45b'
parents = ['f45a']
myfunc = lambda node: node['f45a']
node = Node.FunctionNode(name=name, parents=parents, func=myfunc)
net.nodes.append(node)

# Fifth node
name = 'X5'
params = {"loc": lambda node: node['f45b'], "scale": 1}
parents = ['f45b']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Function 5,6
name = 'f56'
parents = ['X5']
myfunc = lambda node: node['X5']
node = Node.FunctionNode(name=name, parents=parents, func=myfunc)
net.nodes.append(node)

# Sixth node
name = 'X6'
params = {"loc": lambda node: node['X4'] + node['X5'], "scale": 1}
parents = ['X4','f56']
distribution = Distribution.NormalDistribution(params)
evidence = Evidence.Evidence(2,'=')
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution, evidence=evidence)
net.nodes.append(node)

# Function 4,5,b
name = 'fbarren'
parents = ['X6']
myfunc = lambda node: node['X6']
node = Node.FunctionNode(name=name, parents=parents, func=myfunc)
net.nodes.append(node)