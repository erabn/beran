# Sample from CLGexample1 through LikelihoodWeighting

import os 
import timeit
os.chdir("/Users/hfrv2/Documents/beran")
import tests.Resources.CLGexample1 as Examp
import bn.Sampling.LikelihoodWeighting as LW
'''
sample from BN and take the time required for sampling
'''
nsamples = 1000
LWObj = LW.LikelihoodWeighting(Examp.net)
start = timeit.default_timer()
samples, weights = LWObj.sample(amount = nsamples)
end = timeit.default_timer()
print('time[s]: ',end-start)
