from bn.Network import Network
from bn import Node
from bn import Evidence
from bn import Distribution

net = Network()

# First node
name = 'X1'
params = {"loc": 0, "scale": 1}
parents = []
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Second node
name = 'X2'
params = {"loc": 0, "scale": 1}
parents = []
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Third node
name = 'X3'
params = {"loc": lambda samples: samples['X1']+samples['X2'], "scale": 1}
parents = ['X1','X2']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Fourth node
name = 'X4'
params = {"loc": lambda samples: samples['X3'], "scale": 1}
parents = ['X3']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Fifth node
name = 'X5'
params = {"loc": 0, "scale": 1}
parents = ['X3','X4']
distribution = Distribution.NormalDistribution(params)
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution)
net.nodes.append(node)

# Sixth node
name = 'X6'
params = {"loc": lambda samples: samples['X4'] + samples['X5'], "scale": 1}
parents = ['X4','X5']
distribution = Distribution.NormalDistribution(params)
evidence = Evidence.Evidence([2,6],'between')
node = Node.DistributionNode(name=name, parents=parents, distribution=distribution, evidence=evidence)
net.nodes.append(node)