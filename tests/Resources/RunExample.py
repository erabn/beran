# Sample from CLGexample1 through Gibbs sampling

import os 
import timeit
import matplotlib.pyplot as plt 
os.chdir("/Users/kilianzwirglmaier/Documents/beran")
import tests.Resources.CLGexample2 as Examp
import bn.Sampling.GibbsSampling as Gibbs

'''
sample from BN and take the time required for sampling
'''
nsamples = 1000
GibbsObj = Gibbs.GibbsSampler(Examp.net)
start = timeit.default_timer()
samples = GibbsObj.sample(amount = nsamples, burnin = 500)
end = timeit.default_timer()
print('time[s]: ',end-start)

'''
plot histograms for all nodes
'''
samples_lists = {}
for inode in Examp.net.nodes:
	samples_lists.update({inode.name:[]})
	for i in range(0,nsamples):
		samples_lists[inode.name].append(samples[i][inode.name])

	n, bins, patches = plt.hist(samples_lists[inode.name], 50, normed=1)
	plt.xlabel(inode.name)
	plt.ylabel('p')
	plt.show()
