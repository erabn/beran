from bn.Network import Network
from bn import Node
from bn import Evidence
from bn import Distribution
import numpy as np

net = Network()

# First node
name = 'Choice'
states = ['Door1','Door2','Door3']
cpt = [1/3,1/3,1/3]
parents = []
node = Node.DiscreteCPTNode(name=name, parents=parents, cpt=cpt, states=states)
net.nodes.append(node)

# Second node
name = 'Car'
states = ['Door1','Door2','Door3']
cpt = [1/3,1/3,1/3]
parents = []
node = Node.DiscreteCPTNode(name=name, parents=parents, cpt=cpt, states=states)
net.nodes.append(node)

# Third node
name = 'GoatAppeared'
states = ['Door1','Door2','Door3']
cpt = [[[0,0.5,0.5],[0,0,1],[0,1,0]],[[0,0,1],[0.5,0,0.5],[1,0,0]],[[0,1,0],[1,0,0],[0.5,0.5,0]]]
parents = [{'Choice':['Door1','Door2','Door3']},{'Car':['Door1','Door2','Door3']}]
node = Node.DiscreteCPTNode(name=name, parents=parents, cpt=cpt, states=states)
net.nodes.append(node)
