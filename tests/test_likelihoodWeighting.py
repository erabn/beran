from unittest import TestCase
from bn.Sampling.LikelihoodWeighting import LikelihoodWeighting
from tests.Resources import SimpleShip


class TestLikelihoodWeighting(TestCase):
    def setUp(self):
        self.lw = LikelihoodWeighting(SimpleShip.net, 0)

    def test_sample_sanity(self):
        amount = 1000
        samples, weight = self.lw.sample(amount)
        assert weight >= 0 and weight <= 1
        assert len(samples) == amount
