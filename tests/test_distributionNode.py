from unittest import TestCase
from bn.Node import DistributionNode
from bn.Distribution import NormalDistribution
from bn.Evidence import Evidence



class TestDistributionNode(TestCase):
    def setUp(self):
        params = {}
        params['loc'] = 20
        params['scale'] = 5
        distr = NormalDistribution(params)
        self.obj = DistributionNode("Name", [], distr)
        self.samples = [1, 2, 3, 4, 5]

    def test_sample(self):
        sample = self.obj.sample(self.samples)
        assert isinstance(sample, float)

    # This include quite a lot of assertions, because it varies on evidence type
    def test_sample_with_evidence_eq(self):
        self.obj.evidence = Evidence(5, '=')
        sample = self.obj.sample(self.samples)
        assert sample == 5

    def test_sample_with_evidence_lt(self):
        self.obj.evidence = Evidence(16, '<')
        sample = self.obj.sample(self.samples)
        # FIXME: find a normal verification method
        assert sample < 16

    def test_sample_with_evidence_leq(self):
        self.obj.evidence = Evidence(18, '<=')
        sample = self.obj.sample(self.samples)
        assert sample <= 18

    def test_sample_with_evidence_gt(self):
        self.obj.evidence = Evidence(21, '>')
        sample = self.obj.sample(self.samples)
        assert sample > 21

    def test_sample_with_evidence_ge(self):
        self.obj.evidence = Evidence(23, '>=')
        sample = self.obj.sample(self.samples)
        assert sample >= 23

    def test_sample_with_evidence_inrange(self):
        r = [0, 10]
        self.obj.evidence = Evidence(r, 'inrange')
        sample = self.obj.sample(self.samples)
        assert sample >= r[0]
        assert sample <= r[1]

    def test_weight_no_evidence(self):
        weight = self.obj.weight(self.samples)
        assert weight == 1

    def test_weight_with_evidence(self):
        self.obj.evidence = Evidence(5, '=')
        weight = self.obj.weight(self.samples)
        # FIXME: find a normal verification method
        assert isinstance(weight, float)

    def test_distr_boundaries_default(self):
        left, right = self.obj.distr_boundaries()
        assert left == 0
        assert right == 1

    def test_distr_boundaries_left(self):
        left, right = self.obj.distr_boundaries(left=5)
        assert left == self.obj.distribution.cdf(5, [])
        assert right == 1

    def test_distr_boundaries_right(self):
        left, right = self.obj.distr_boundaries(right=5)
        assert left == 0
        assert right == self.obj.distribution.cdf(5, [])

    def test_distr_boundaries_both(self):
        left, right = self.obj.distr_boundaries(left=1, right=2)
        assert left == self.obj.distribution.cdf(1, [])
        assert right == self.obj.distribution.cdf(2, [])

    def test_range_rand(self):
        r = self.obj.range_rand()
        assert isinstance(r, float)
