from unittest import TestCase
from bn.Node import Node
from bn.Evidence import Evidence


class TestNode(TestCase):
    def setUp(self):
        # Prepare data
        self.data = {'name': 'NodeTest',
                     'parents': [1, 2, 3],
                     'evidence': Evidence(5, '<')}


    def test_types(self):
        # Generally we just care that children is a list and evidence is Evidence instance
        obj = Node(self.data['name'], self.data['parents'], self.data['evidence'])
        assert obj.name == self.data['name']
        assert obj.parents == self.data['parents']
        assert isinstance(obj.parents, list)
        assert isinstance(obj.evidence, Evidence)

    def test_evidence_none(self):
        # Evidence should only be of instance - Evidence
        obj = Node(self.data['name'], self.data['parents'])
        assert obj.evidence == None
