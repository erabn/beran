from unittest import TestCase
from bn.Node import FunctionNode
from bn.Evidence import Evidence

class TestFunctionNode(TestCase):
    def setUp(self):
        def func(samples): return 2 * samples[2] + samples[1]
        self.func = func
        self.obj = FunctionNode("Name", [], func)
        self.samples = [1, 2, 3, 4, 5]

    def test_sample_with_evidence(self):
        evid_val = 5
        evid_type = '='
        self.obj.evidence = Evidence(evid_val, evid_type)
        sample = self.obj.sample(self.samples)
        assert sample == evid_val


    def test_sample_without_evidence(self):
        sample = self.obj.sample(self.samples)
        assert sample == self.func(self.samples)

    def test_weight_with_evidence_unsatisfied(self):
        # Intentionally make evidence different
        evid_val = self.func(self.samples) - 1
        evid_type = '='
        self.obj.evidence = Evidence(evid_val, evid_type)
        weight = self.obj.weight(self.samples)
        assert weight == 0

    def test_weight_with_evidence_satisfied(self):
        # Intentionally make evidence different
        evid_val = self.func(self.samples)
        evid_type = '='
        self.obj.evidence = Evidence(evid_val, evid_type)
        weight = self.obj.weight(self.samples)
        assert weight == 1

    def test_weight_without_evidence(self):
        weight = self.obj.weight(self.samples)
        assert weight == 1