from random import random
from unittest import TestCase

from bn.Distribution import CPTDistribution


class TestCPTDistribution(TestCase):
    def setUp(self):
        # some table
        cpt = [[0.5, 0.3, 0.2, 0.1, 1, 0],
               [0.5, 0.3, 0.5, 0.8, 0, 0.3],
               [0, 0.4, 0.3, 0.1, 0, 0.7]]
        states = [0, 5, 10]
        pstates = [[1, 2, 3, 1, 2, 3],
                   [4, 5, 4, 5, 4, 5]]
        parents = ['a', 'b']

        self.obj = CPTDistribution(cpt=cpt, states=states, parent_states=pstates, parents=parents)

    def test_get_column(self):
        # Iterate through parent states and check all columns
        size = len(self.obj.params['parent_states'][0])
        for i in range(size):
            samples = {'a': self.obj.params['parent_states'][0][i],
                       'b': self.obj.params['parent_states'][1][i]}
            column = self.obj.get_column(samples)
            assert column == i

    def test_icdf(self):
        # Single random, iterative approach
        r = random()
        size = len(self.obj.params['parent_states'][0])
        for i in range(size):
            samples = {'a': self.obj.params['parent_states'][0][i],
                       'b': self.obj.params['parent_states'][1][i]}
            res = self.obj.icdf(r, samples)
            assert (res in self.obj.params['states'])

    def test_pdf(self):
        # Single random, iterative approach
        size = len(self.obj.params['parent_states'][0])
        # Iterate through parent sample combinations
        for i in range(size):
            # Iterate through possible states
            samples = {'a': self.obj.params['parent_states'][0][i],
                       'b': self.obj.params['parent_states'][1][i]}
            for s_i, state in enumerate(self.obj.params['states']):
                res = self.obj.pdf(state, samples)
                assert res == self.obj.params['cpt'][s_i][i]


